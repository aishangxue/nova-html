<?php

namespace Sweetycode\NovaHtml;

use Laravel\Nova\Fields\Expandable;
use Laravel\Nova\Fields\Field;

class NovaHtml extends Field
{
    use Expandable;

    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova-html';

    public $showOnIndex = false;

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'shouldShow' => $this->shouldBeExpanded(),
        ]);
    }
}
