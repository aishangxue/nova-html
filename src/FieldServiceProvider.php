<?php

namespace Sweetycode\NovaHtml;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::remoteScript('https://cdn.staticfile.org/tinymce/5.0.16/tinymce.min.js');
            Nova::script('nova-html', __DIR__.'/../dist/js/field.js');
            Nova::style('nova-html', __DIR__.'/../dist/css/field.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
