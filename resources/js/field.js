Nova.booting((Vue, router, store) => {
  Vue.component('index-nova-html', require('./components/IndexField'))
  Vue.component('detail-nova-html', require('./components/DetailField'))
  Vue.component('form-nova-html', require('./components/FormField'))
})
